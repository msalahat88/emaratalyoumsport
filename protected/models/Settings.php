<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property integer $how_many_posts
 * @property string $start_date_time
 * @property string $end_date_time
 * @property string $direct_push_start
 * @property string $direct_push_end
 * @property integer $gap_time
 * @property integer $jobs_posts_per_day
 * @property integer $jobs_posts_per_week
 * @property string $updated_at
 * @property string $created_at
 * @property string $date_check_job
 * @property string $timezone
 * @property int $category_id
 * @property int $direct_push
 * @property int $generator_is_running
 * @property int $pinned
 */
class Settings extends BaseModels
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gap_time, start_date_time, end_date_time, created_at', 'required'),
			array('direct_push_start, direct_push_end', 'check_direct'),
			array('how_many_posts,gap_time,jobs_posts_per_week,jobs_posts_per_day', 'numerical', 'integerOnly'=>true),
			//array('end_date_time','compare','compareAttribute'=>'start_date_time','operator'=>'>','message'=>'Start Date must be less than End Date'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, how_many_posts, start_date_time, pinned, end_date_time, updated_at, created_at', 'safe'),
		);
	}
	public function check_direct($att){
		if($this->direct_push){
			if(empty($this->direct_push_start)){
				$this->addError('direct_push_start', 'Start direct push cannot be empty');
			}

			if(empty($this->direct_push_end)){
				$this->addError('direct_push_end', 'End direct push cannot be empty');
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cat' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timezone' =>'TimeZone',
			'how_many_posts' => 'Posts Per day',
			'start_date_time' => 'Posting starts at',
			'end_date_time' => 'Posting ends at',
			'updated_at' => 'Updated At',
			'gap_time' => 'Time Gap ',
			'jobs_posts_per_day' =>'Posts per day',
			'date_check_job' => 'date_check_job',
			'jobs_posts_per_week'=>'Gap between posting days',
 			'created_at' => 'Created At',
 			'category_id' => 'Category',
			'direct_push' =>'Direct Push',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($type = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('how_many_posts',$this->how_many_posts);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('start_date_time',$this->start_date_time,true);
		$criteria->compare('end_date_time',$this->end_date_time,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('gap_time',$this->gap_time,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('jobs_posts_per_week',$this->jobs_posts_per_week,true);
		$criteria->compare('jobs_posts_per_day',$this->jobs_posts_per_day,true);
		$criteria->compare('date_check_job',$this->date_check_job,true);

		if($type)
		$criteria->condition = 'category_id is not NULL ';

 		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function all_numbers(){
		$data=array();
		for($i=1;$i<=100;$i++){
			$data[$i]=$i;
		}
		return $data;
	}

	public function posts_per_week(){
		$data=array();
		for($i=1;$i<=7;$i++){
			$data[$i]=$i;
		}
		return $data;
	}
	public function gap_time(){
		$data=array();
		for($i=0;$i<=120;$i+=5){
			if($i == 60)
				$data[$i]=$i.' minute (One hour) ';
			elseif($i == 120)
				$data[$i]=$i.' minute  (Two hour) ';
			else
			$data[$i]=$i.' minute ';
		}
		return $data;
	}

	public function get_category(){

		$settings = $this->findAll('category_id is not NULL');
		$category = CHtml::listData(Category::model()->get_category(false),'id','title');
		foreach ($settings as $set) {
			if(array_key_exists($set->category_id,$category)){
				 unset($category[$set->category_id]);
			}
		}
		return $category;
	}

	public function getTimeZone(){
		$locations = array();
		foreach (timezone_identifiers_list() as $zone)
			$locations[$zone]=$zone;

		return $locations;
	}

	public function get_generator_is_running(){

		$settings = $this->findByPk(1);
		if(!empty($settings)){
			return $settings->generator_is_running;
		}
		return false;
	}
}

<?php

class GeneratorsCommand extends BaseCommand
{
    // Please to use this command , run every 15 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom;

    private $rule;

    private $template;

    private $main_hash_tag;

    protected  $hash_tag;

    protected $trim_hash;

    private $news_counter = 0;

    private $fb_is_scheduled = true;

    private $twitter_is_scheduled = true;

    private $instagram_is_scheduled = true;

    private $section_name = 'general';

    protected $category;

    protected $sub_category;

    private $settings;

    private $settings_category;

    public function run($args)
    {
        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {
            echo 'Please insert template ' . PHP_EOL;
            exit();
        }

        system("clear");

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index => $item) {
            $this->trim_hash[$index] = " #" . str_ireplace(" ", "_", trim($item->title)) . ' ';
            $this->hash_tag[$index] = ' ' . trim($item->title) . ' ';
        }
        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->sub_category = SubCategories::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        $this->settings_category = Settings::model()->findAll();

        /** add cron*/

        $this->rssFeed();
    }

    private function rssFeed()
    {

        if (!is_array($this->sub_category)) {
            exit();
        }

        foreach ($this->sub_category as $sub) {

            $html = new SimpleHTMLDOM();
            $html_data = $html->file_get_html($sub->url);

            if(empty(trim($html_data))){
                continue;
            }
            $programs = $html_data->find('div[class=m_overlay] p[class=heading overleytitle] a');

            foreach ($programs as $program) {

                $data['date']= $this->get_date_from_url($program->href);

               if(date('Y-m-d',strtotime($data['date'])) == date('Y-m-d')){

                   $data['sub_category_id'] = $sub->id;

                   $data['category_id'] = $sub->category_id;

                   $data['sub'] = $sub;

                   $data['creator'] = null;

                   $data['column'] = null;

                   $data['num_read'] = null;

                   $data['title'] = null;

                   $data['link'] = null;

                   $data['link_md5'] = null;

                   $data['description'] = null;

                   $data['body_description'] = null;

                   $data['shorten_url'] = null;

                   $data['publishing_date'] = null;

                   $data ['link'] = null;

                   $data ['link_md5'] = null;

                   if(isset($program->href)) {

                       $data ['link'] = Yii::app()->params['feedUrl'] . $program->href;

                       $data ['link_md5'] = md5($data ['link']);

                       list($title, $author, $description, $image, $column, $gallery) = $this->get_details_ge($data ['link']);

                       $data['title'] = $title;

                       $data['publishing_date'] = $data['date'];

                       $data['creator'] = $author;

                       $data['description'] = $description;

                       $data['column'] = $column;

                       $data['media']['image'] = $image;

                       $data['media']['gallery'] = $gallery;


                       $this->settings_time($data['category_id']);

                       $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));

                       if (empty($news)) {

                           $this->news_counter++;

                           $this->AddNews($data);

                       } else {
                           echo 'News is exist ' . PHP_EOL;
                       }

                   }
               }
            }

        }

    }

    private function get_details_ge($url)
    {
        $html = new SimpleHTMLDOM();

        $html_data = $html->file_get_html($url);

        $title=null;

        $author=null;
        $number_star=null;
        $description=null;
        $image=null;
        $column=null;
        $gallery=null;
        $news_date =null;
        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'last-modified') !== false)
                $news_date =  date('Y-m-d H:i:s',strtotime($item->getAttribute('content')));
        }
        if(isset($html_data->find('h1[class=articletitle]', 0)->innertext))
            $title= $this->clear_tags($html_data->find('h1[class=articletitle]', 0)->innertext);
        else {
            if(isset($html_data->find('meta[property=og:title]',0)->content))
                $title=  $this->clear_tags($html_data->find('meta[property=og:title]',0)->content);
            else
                $title = 'blank title';
        }

        if(isset($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext))
            $author = $this->clear_author($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext);
        else{
            if(isset($html_data->find('meta[name=author]',0)->content))
                $author = $this->clear_author($html_data->find('meta[name=author]',0)->content);
            elseif(isset($html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content))
                $author = $html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content;
            else
                $author = 'blank author';
        }


        /*if(isset($html_data->find('span[id=star-raters-number]', 0)->innertext))
            $number_star = $this->clear_tags($html_data->find('span[id=star-raters-number]', 0)->innertext);*/


        if(isset($html_data->find('div[id=article-body] p', 0)->innertext))
            $description= $this->clear_tags($html_data->find('div[id=article-body] p', 0)->innertext);

        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $image = $item->getAttribute('content');
            }
        }
        $gallary = $html_data->find('div[class=imagegalleryslider] ul[class=slides] li img');
        $gallary2 = $html_data->find('div[class=articleinlinegallery] ul[class=slides] li img');
        if(!empty($gallary) and is_array($gallary)){
            foreach ($gallary as $item) {
                if($image != Yii::app()->params['feedUrl'].ltrim($item->src,'/'))
                $gallery[] = Yii::app()->params['feedUrl'].ltrim($item->src,'/');
            }

        }elseif(!empty($gallary2) and is_array($gallary2)){
            foreach($gallary2 as $item){
                if($image != Yii::app()->params['feedUrl'].ltrim($item->src,'/'))
                    $gallery[] = Yii::app()->params['feedUrl'].ltrim($item->src,'/');
            }
        }

        if(isset($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext))
            $column = $this->clear_tags($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext);

        return array($title,$author,$description,$image,$column,$gallery,$news_date);
    }

    private function settings_time($category_id)
    {

        $catch = false;
        if ($this->section_name != 'general') {
            $category_id = 1;
        }

        foreach ($this->settings_category as $item) {
            if ($item->category_id == $category_id) {
                $settings_category = $item;
                $catch = true;
                break;
            }
        }

        if (!empty($settings_category) && $catch) {
            $this->start_time = $settings_category->start_date_time;
            $this->end_time = $settings_category->end_date_time;
            $this->gap_time = $settings_category->gap_time;
            $this->direct_push_start = $settings_category->direct_push_start;
            $this->direct_push_end = $settings_category->direct_push_end;
            $this->is_custom_settings = true;
            $this->is_direct_push = $settings_category->direct_push;
        } else {
            $this->start_time = $this->settings->start_date_time;
            $this->end_time = $this->settings->end_date_time;
            $this->gap_time = $this->settings->gap_time;
            $this->direct_push_start = $this->settings->direct_push_start;
            $this->direct_push_end = $this->settings->direct_push_end;
            $this->is_custom_settings = false;
            $this->is_direct_push = $this->settings->direct_push;
        }
        $type = 'general';
        if ($catch)
            $type = 'custom';

        return $type;
    }

    private function date($cat_id)
    {
        $final_date = null;
        $condition = new CDbCriteria();
        $condition->order = 'id Desc';
        $condition->limit = 1;

        if ($this->section_name != 'general') {
            $cat_id = 1;
        }

        if ($this->is_custom_settings) {
            $condition->condition = 'is_scheduled = 1 and settings="custom" and main_category_id = ' . $cat_id;
        } else {
            $condition->condition = 'is_scheduled = 1 and settings="general"';
        }

        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);

        $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
        $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }

        if (empty($check)) {
            if (($now <= $start)) {
                $final_date = $start;

            } elseif ($now <= $end) {
                $final_date = $now;
            } else {
                $final_date = $now;
                $is_scheduled = 0;
            }
        } else {
            $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
            if ($date < $now) {
                $date = $now;
            }

            if (($date <= $start)) {
                $final_date = $start;
            } elseif ($date <= $end) {
                $final_date = $date;
            } else {
                $final_date = $date;
                $is_scheduled = 0;
            }
        }

        if ($this->is_direct_push) {
            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:s'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:s') . ' -1 minute');
                $is_scheduled = 1;
            }
        }

        $post_date = date('Y-m-d H:i:s', $final_date);

        return array($post_date, $is_scheduled);

    }

    private function AddNews($data)
    {
        $news = new News();

        $news->id = null;

        $news->link = $data['link'];

        $news->link_md5 = $data['link_md5'];

        $news->category_id = $data['category_id'];

        $news->sub_category_id = $data['sub_category_id'];

        $news->title = $data['title'];

        $news->column = $data['column'];

        $news->description = $data['description'];

        $news->publishing_date = $data['publishing_date'];

        $news->schedule_date = $data['publishing_date'];

        $news->created_at = date('Y-m-d H:i:s');

        $news->creator = $data['creator'];

        $shortUrl = new ShortUrl();

        $news->shorten_url = $shortUrl->short(urldecode($news->link));

        list($post_date, $is_scheduled) = $this->date($data['category_id']);

        $news->schedule_date = $post_date;

        $news->setIsNewRecord(true);

        if ($news->save(true)) {

            $id = null;

            if (isset($data['media']['video']))
                $id = $this->AddMediaNews($news->id, 'video', $data['media']['video']);

            if (isset($data['media']['image']))
                $id = $this->AddMediaNews($news->id, 'image', $data['media']['image']);

            if (isset($data['media']['gallery']))
                $id = $this->AddMediaNews($news->id, 'gallery', $data['media']['gallery']);

            $this->newsGenerator($news, isset($data['media']['gallery']) ? 'gallery' : 'image',$id, $is_scheduled);

        } else {
            echo "News Already Exist in the database" . PHP_EOL;
        }

    }

    public function AddMediaNews($news_id, $type, $data)
    {

        $id = null;
        if ($type == 'gallery') {
            foreach ($data as $item) {
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item, 'news_id' => $news_id, 'type' => $type)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item;
                    $media->news_id = $news_id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news_id, 'type' => $type)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news_id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;
        }

        return $id;


    }

    protected function newsGenerator($news, $type,$id, $is_scheduled)
    {

        $this->scheduled_counter++;

        $parent = null;

        if ($this->scheduled_counter > $this->per_post_day) {
            $is_scheduled = 0;
        }

        $category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));

        $sub_category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->subCategory->title));


        foreach ($this->PlatFrom as $item) {

            $twitter_is_scheduled = false;

            $media = null;

            $temp = $this->get_template($item, $news->category_id, $type);

            $title = $news->title;

            $description = $news->description;

            if ($item->title != 'Instagram') {
                $title = str_ireplace($this->hash_tag, $this->trim_hash, $news->title);
                $description = str_ireplace($this->hash_tag, $this->trim_hash, $news->description);
            }

            $title = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);

            $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

            $description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);

            $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

            $shorten_url = $news->shorten_url;

            $full_creator = $news->creator;

            $text = str_ireplace(
                array('[title]', '[description]', '[short_link]', '[author]'),
                array($title, $description, $shorten_url, $full_creator),
                $temp['text']
            );

            if ($item->title == 'Facebook' or $item->title == 'Twitter') {

                $text = str_ireplace('# ', '#', $text);

                $found = true;

                preg_match_all("/#([^\s]+)/", $text, $matches);

                if (isset($matches[0])) {
                    $matches[0] = array_reverse($matches[0]);
                    $count = 0;
                    foreach ($matches[0] as $hashtag) {
                        if ($count >= 2) {
                            $found = false;
                            $hashtag_without = str_ireplace('#', '', $hashtag);
                            $hashtag_without = str_ireplace('_', ' ', $hashtag_without);
                            $text = str_ireplace($hashtag, $hashtag_without, $text);

                        }
                        $count++;
                    }

                    if ($count >= 2) {
                        $found = false;
                    }
                }

                if ($found) {
                    $text = str_ireplace(array('[section]', '[sub_section]'), array($category, $sub_category,), $text);
                } else {
                    $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
                }

            } elseif ($item->title == 'Instagram') {
                $text = str_ireplace('# ', '#', $text);
                $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
            }


            $newsMedia = MediaNews::model()->findByPk($id);

            if(!empty($newsMedia))
                $media = $newsMedia->media;

            if ($item->title == 'Twitter') {

                $text_twitter = $text;
                if ($temp['type'] == 'Preview')
                    if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                        $text = $text . PHP_EOL . $news->shorten_url;


                if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141) {
                    $is_scheduled = 0;
                } else {
                    $text = $text_twitter . PHP_EOL;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text .= PHP_EOL . $news->shorten_url;
                }
            } else {
                if ($twitter_is_scheduled) {
                    if (!$is_scheduled) {
                        $is_scheduled = 1;
                    }
                }
            }

            $text = str_ireplace('# #', '#', $text);
            $text = str_ireplace('##', '#', $text);
            $text = str_ireplace('&#8220;', '', $text);
            $text = str_ireplace('&#8221;', '', $text);
            $text = str_ireplace('&#8230;', '', $text);
            $text = str_ireplace('&#x2014;', '', $text);
            $text = str_ireplace('#8211;', '', $text);
            $text = str_ireplace('&#8211;', '', $text);
            $text = str_ireplace('&nbsp;', '', $text);
            $text = str_ireplace('&#160;', '', $text);
            $text = str_ireplace('. .', '', $text);
            $text = str_ireplace('&#xf2;,', '', $text);
            $text = str_ireplace('&#8211;', '', $text);
            $text = str_ireplace('&ndash;', '', $text);
            if ($item->title != 'Instagram') {
                $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
            }


            if ($item->title == 'Twitter') {
                if (!$this->twitter_is_scheduled)
                    $is_scheduled = 0;
            }


            if ($item->title == 'Instagram') {
                if (!$this->instagram_is_scheduled)
                    $is_scheduled = 0;
            }


            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id = null;
            $PostQueue->command = false;
            $PostQueue->type = $temp['type'];
            $PostQueue->post = trim($text);
            $PostQueue->schedule_date = $news->schedule_date;
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->main_category_id = $this->section_name != 'general' ? 1 : $news->category_id;
            $PostQueue->link = $news->shorten_url;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id = $news->id;
            $PostQueue->post_id = null;
            $PostQueue->media_url = $media;
            $PostQueue->settings = $this->is_custom_settings ? 'custom' : 'general';
            $PostQueue->is_scheduled = $is_scheduled;
            $PostQueue->platform_id = $item->id;
            $PostQueue->generated = 'auto';
            $PostQueue->created_at = date('Y-m-d H:i:s');
            if ($parent == null) {
                $PostQueue->parent_id = null;
                if ($PostQueue->save())
                    $parent = $PostQueue->id;
                else
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');

            } else {
                $PostQueue->parent_id = $parent;
                if (!$PostQueue->save())
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');
            }

        }

        $news->generated = 1;
        if (!$news->save())
            $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '"></a>');


    }

    private function get_template($platform, $category, $type)
    {

        $temp = PostTemplate::model()->findByAttributes(array(
            'platform_id' => $platform->id,
            'catgory_id' => $category,
        ));



        if (!empty($temp))
            return $temp;

        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp = PostTemplate::model()->find($cond);

        if (!empty($temp))
            return $temp;

        if ($platform->title == 'Instagram') {
            isset($temp->id) ? $temp->id : null;
        }

        return Yii::app()->params['templates'];
    }

    private function get_date_from_url($url)
    {
        $url = explode('/', $url);
        if (is_array($url)) {
            $url = explode('-', end($url));
            if (is_array($url) && (isset($url[0]) && isset($url[1]) && isset($url[2])) && (!empty($url[0]) && !empty($url[1]) && !empty($url[2]))) {
                return date('Y-m-d', strtotime($url[0] . '-' . $url[1] . '-' . $url[2]));
            }
        }
        return false;
    }


}
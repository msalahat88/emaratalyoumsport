<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form TbActiveForm */
?>
<?php
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'id'=>'settings-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
));
$ones= array();
for($one = 0 ; $one < 51 ;$one++){
    $ones[$one]=$one;
}
?>
<?php echo $form->errorSummary($model); ?>

    <?php 	echo $form->dropDownListGroup($model,'gap_time',array(
        'widgetOptions'=>array(
            'data'=>$model->gap_time(),
        ),
    )); ?>

<?php echo $form->timeFieldGroup($model,'start_date_time',array(
        'widgetOptions'=>array(
            'htmlOptions'=>array('readOnly'=>true,'class'=>'time_start')
        )
    )
); ?>

<?php echo $form->timeFieldGroup($model,'end_date_time',array(
    'widgetOptions'=>array(
        'htmlOptions'=>array('readOnly'=>true,'class'=>'time_end')
    )
)); ?>

<?php
$category = CHtml::listData(Category::model()->get_category(false),'id','title') ;
$html =array('ReadOnly'=>'ReadOnly','disabled'=>'disabled');
if($model->isNewRecord){

    $category = $model->get_category() ;
    $html = array();
}
echo   $form->dropDownListGroup($model,'category_id',array(
    'widgetOptions'=>array(
        'data'=>$category,
        'empty'=>'Choose One',
        'htmlOptions'=>$html

    ),
)) ; ?>

<div class="form-actions  pull-right" style="margin-bottom: 20px">
    <?php
    echo !$model->isNewRecord ? CHtml::link('Remove', array('settings/delete','id'=>$model->id),
        array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','class'=>'btn btn-danger','style'=>'margin-right: 14px;')
    ): '';
    $this->widget(
        'booster.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save'
        )
    );

    ?>
</div>
<?php $this->endWidget(); ?>
<script>
    $( document ).ready(function() {
        $('#Settings_start_date_time').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
        $('#Settings_end_date_time').datetimepicker({
            datepicker:false,
            format:'H:i:00',
            step:30
        });
    });
</script>
<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $sub_Sections SubCategories */
/* @var $news_table News */

$this->pageTitle = "Sections | View";

$this->breadcrumbs = array(
    'sections' => array('admin'),

    $model->title,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerCss('mycss', '
   .grid-view {
padding-top: 0px;
}
.detail-view th {
    text-align: left;
    width: 160px;
}
.sub_category{
    border-left: 1px solid #E2E2E2;}
.box-body{
    border-bottom: 1px solid #E2E2E2;

}
#subSection-grid_c0 .sort-link{
color:Black;
font-size:16px;

}

');
?>
<section class="content">
    <?PHP if(Yii::app()->user->hasFlash('create')){ ?>
        <div class="callout callout-success" style="margin-top:20px;">
            <h4>created successfully. </h4>
        </div>

    <?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
        <div class="callout callout-info" style="margin-top:20px;">
            <h4>updated successfully. </h4>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-sm-12 pull-right">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>
                        <?PHP
                        $this->widget(
                            'booster.widgets.TbButtonGroup',
                            array(
                                'size' => 'small',
                                'context' => 'info',
                                'buttons' => array(
                                    array(
                                        'label' => 'Action',
                                        'items' => array(
                                            array('label' => 'Update', 'url' => array('update', 'id' => $model->id)),
                                            array('label' => 'Delete', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
                                            array('label' => 'Manage', 'url' => array('admin')),
                                        ),
                                    ),

                                ),
                            )
                        );
                        ?>
                    </div>
                    <div class="col-sm-6" style="margin-top:10px">
                        <?PHP
                        $previous_items= $model->getPreviousId();

                        if(isset($previous_items)) {
                            $title = $previous_items->title;
                            $id = $previous_items->id;


                            $this->widget(
                                'booster.widgets.TbButtonGroup',
                                array(
                                    'size' => 'small',
                                    'context' => 'warning',
                                    'buttons' => array(


                                        array('label' => $title,
                                            'buttonType' => 'link',
                                            'url' => array('view', 'id' => $id)),


                                    ),
                                )
                            );
                        }
                        ?>
                    </div>
                    <div class="col-sm-6"style="margin-top:10px;    text-align: right;">
                        <?PHP
                        $next_items= $model->getNextId();

                        if(isset($next_items)) {
                            $title = $next_items->title;
                            $id = $next_items->id;


                            $this->widget(
                                'booster.widgets.TbButtonGroup',
                                array(
                                    'size' => 'small',
                                    'context' => 'warning',
                                    'buttons' => array(


                                        array('label' => $title,
                                            'buttonType' => 'link',
                                            'url' => array('view', 'id' => $id)),


                                    ),
                                )
                            );
                        }
                        ?>
                    </div>
                </div>

            <div class="box-body">
                <div class=" col-sm-8">
                    <?php $this->widget('booster.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            array(
                                'name' => 'title',
                                'type' => 'raw',
                                'value' => CHtml::link($model->title, $model->url, array('target' => '_blank')),
                            ),
                            array(
                                'name' => 'active',
                                'value' => $model->active == 1 ? "active" : "disabled",
                            ),
                            'created_at',
                        ),
                    )); ?>
                </div>
                <div class=" col-sm-4 sub_category">
                    <?php
                    $this->widget('booster.widgets.TbGridView', array(
                        'id' => 'subSection-grid',

                        'template' => '{items}{pager}',
                        'dataProvider' => $sub_Sections->search(true),
                        'columns' => array(
                            array(
                                'name' => 'title',
                                'header' => 'Sub Section',
                                'value' => $sub_Sections->title,
                            ),
                        ),
                    )); ?>
                </div>


                    <?PHP
                    $this->widget('booster.widgets.TbGridView', array(
                        'id' => 'News-grid',
                        'type' => 'striped bordered condensed',
                        'template' => '{pager}{items}{summary}{pager}',
                        'enablePagination' => true,
                        'pager' => array(
                            'class' => 'booster.widgets.TbPager',

                            'htmlOptions' => array(
                                'class' => 'pull-right'
                            )
                        ),
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'dataProvider' => $news_table->search(false, false),
                        'columns' => array(
                            array(
                                'name' => 'id',
                                'value' => $news_table->id,
                            ),
                            array(
                                'name'=>'title',
                                'type'=>'raw',
                                'value'=>'CHtml::link($data->title,$data->shorten_url,array(\'target\'=>\'_blank\'))',
                            ),
                            array(
                                'name' => 'schedule_date',
                                'value' => $news_table->schedule_date,
                            ),
                            array(
                                'class' => 'booster.widgets.TbButtonColumn',
                                'header' => 'Options',
                                'template' => '{view}',
                                'buttons' => array(
                                    'view' => array(
                                        'label' => 'View',
                                        'url' => 'Yii::app()->createUrl("news/view", array("id"=>$data->id))',
                                        'icon' => 'fa fa-eye',
                                    ),

                                )
                            ),
                        )));
                    ?>

            </div>
        </div>
    </div>
</section>


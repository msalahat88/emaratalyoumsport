<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List News', 'url'=>array('index')),
	array('label'=>'Create News', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#news-form').change(function(){

    $(this).submit();

});
$('.search-form form').submit(function(){
	$('#news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">

					<div class="col-md-12 pull-right"><h2>
							<?php echo Yii::app()->params['statement']['previousPage']; ?>
						</h2></div>
					<?php
					$form = $this->beginWidget('booster.widgets.TbActiveForm',
						array(
							'id' => 'news-form',
							'type' => 'horizontal',
							//'action' => Yii::app()->createUrl('admin'),
							'method' => 'get',
						)
					);
					?>
					<div class="col-sm-4 pull-left "  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
	<div class="col-sm-2 ">


						<?PHP
						echo $form->datePickerGroup(
							$model,
							'from',
							array(
								'widgetOptions' => array(
									'options' => array(
										'viewformat' => 'yyyy-mm-dd',
										'format' => 'yyyy-mm-dd',
									),
								),
								'labelOptions' => array(
									'class' => 'col-sm-3 '
								),
								'wrapperHtmlOptions' => array(
									'class' => 'col-sm-9 ',
									'style' => 'padding-left: 14px;'
								),
							)
						);


						?></div>
					<div class="col-sm-2">

						<?PHP
						echo $form->datePickerGroup(
							$model,
							'to',
							array(
								'widgetOptions' => array(
									'options' => array(
										'viewformat' => 'yyyy-mm-dd',
										'format' => 'yyyy-mm-dd',
									)
								),
								'labelOptions' => array(
									'class' => 'col-sm-3 '
								),
								'wrapperHtmlOptions' => array(
									'class' => 'col-sm-9 ',
									'style' => 'padding-left: 14px;'
								),
							)
						);
						?></div>
					<?PHP $this->endWidget(); ?>


				</div>
				<div class="box-body">
					<?PHP
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'news-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(),
						'filter' => $model,
						'columns' => array(

							'title',
							array(
								'name'=>'shorten_url',
								'type'=>'raw',
								'value'=>'CHtml::link($data->shorten_url,$data->shorten_url,array("target"=>"_blank"))',
								'header' => 'Url',

								),
							'description',
							'schedule_date',
							'publishing_date',
							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),


								)
							),



						)));?>

				</div>
			</div>
				</div>
			</div>
		</div>
</section>


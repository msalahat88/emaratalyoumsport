<?php

class DefaultController extends BaseController
{
	public function actionIndex()
	{


		$this->render('index',array(
			'server_memory'=>$this->convert(memory_get_usage(true)),
			'server_cpu'=>$this->get_server_cpu_usage(),
			'category'=>Category::model()->get_category(),
			'all_news'=>News::model()->get_all_news(),
		));
	}

	function convert($size)
	{
		$unit=array('b','kb','Mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	function get_server_cpu_usage(){

		$load = sys_getloadavg();
		return $load[0];

	}

}
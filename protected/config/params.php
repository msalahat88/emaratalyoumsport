<?php
// This is the database connection configuration.
return array(
    // this is used in contact page
    'adminEmail'=>'webmaster@example.com',
    'webroot'   => '/var/www/html/emaratalyoum', // change this on production server
    'domain'   => 'http://apps.sortechs.com/emaratalyoum/', // change this on production server
    'amazonBucket' =>'sortechs',
    'amazonAccessKey' => 'AKIAJUNLV2HM7F4JO22A',
    'amazonSecretKey' => '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',
    'amazonFolder' => 'emaratalyoum',
    'amazonURL' => 'https://s3.amazonaws.com/',
    'feedUrl' => 'http://www.emaratalyoum.com/',
    'feedUrlRss' => 'http://www.emaratalyoum.com/rss-7.166',
    'HashTagRoot'=>array(
        'ar'=>'#الامارات_اليوم',
        'en'=>'#emaratalyoum'
    ),
    'line'=>'بقلم',
    'source'=>'المصدر',

    //Facebook config
    'facebook'=>array(
        'LinkPage'=>'https://www.facebook.com/Sortech-emsport-1218227198210170/',
        'page_id'=>'1218227198210170', // Your facebook Page ID
        'app_id'=>'985854914794106', // Your facebook app ID
        'secret'=>'049117c99fc665419e3301ba81b96bfe', // Your facebook secret
        'token'=>'CAAOAoTvZCEnoBAFarzGvRdDjG03ouNkiK5NYyj7aO3XvRTmGZAUrFPNrD3RWkVfV12iNpgjUMvZBHf0D1JsuKJNawjsZBOHM1FPw26uOoBMOgvZBEewy7ygpzQZCVZCAl4rn3cOk1FP5hS5uXzZByzVX57WdndvqPi3x8IvWmUp8vwAgLh7zNle5rc480F2tZCkgyAaCBUe4UcwZDZD', // The access token you receieved above

    ),

    //Instagram config
    'instagram_auth'=>array(
        'LinkPage'=>'https://www.instagram.com/sortechs_ems/',
        'username' => 'sortechs_ems',
        'password' => 'nusrv123456',
        'debug'    => true
    ),

    //twitter config

    'twitter'=>array(
        'LinkPage'=>'https://twitter.com/sortechs_ESport',
        'key'=>'fPZLFtJXoBzf3MotdYd1YQEoo',
        'secret'=>'U7gldK8UWmSHpK1TzOeIiaU7TCmsj8GZLpMr6srwSWztjFsRmj',
        'token'=>'757991605743788032-YE1LnqfYI6DVf1UU2uzL2FKQPzh8rfs',
        'token_secret'=>'4EQ78cLztlWJ0QSfoYj9EyQS0YXKSJObPRUjgymx8OHDg',
    ),

    'templates'=>array(
        'text'=>'[title]'.PHP_EOL.'[short_link]'.PHP_EOL.'[section]',
        'type'=>'Preview',
    ),

    'rules'=>array(
        'facebook' =>array(
            'Preview',
            'Preview',
            /*'text',*/
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',

            /*'image',
            'image',
            'image',*/
            /*'text',*/
            'image',
            'image',
            //'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(

            'Preview',
            'Preview',
            /*'text',*/
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',

            /*'image',
            'image',
            'image',*/
            /*'text',*/
            'image',
            'image',
            //'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',


        ),
        'instagram'=> array(
            'image',
            // 'video',
        )
    ),

    'rules_hashTag'=>'main',
    'other_hahTag'=>'hash_tag',

    'shorten_url'=>'للمزيد من التفاصيل :',

    'rule_array'=>array(
        'facebook' =>array(
            'text'=>'text',
            'preview'=>'preview',
            'image'=>'image',
            'video'=>'video',
            'youtube'=>'youtube',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(
            'text'=>'text',
            'preview'=>'preview',
            'image'=>'image',
            'video'=>'video',
            'youtube'=>'youtube',
        ),
        'instagram'=> array(
            'image'=>'image',
        )
    ),

    'rule_template'=>array(
        'facebook' =>array(
            'image'=>'image',
            'text'=>'text',
            'preview'=>'preview',

            'video'=>'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(
            'image'=>'image',
            'text'=>'text',
            'preview'=>'preview',

            'video'=>'video',
        ),
        'instagram'=> array(
            'image'=>'image',
        )
    ),

    'rule_filter'=>array(
        ''=>'',
        'text'=>'text',
        'preview'=>'preview',
        'image'=>'image',
        'video'=>'video',
    ),

    'statement'=>array(
        'previousPage'=>CHtml::button('Previous Page',array('onClick'=>'history.go(-1)','class'=>'btn btn-primary btn-sm pull-right')),
    ),

    /*'email'=>array(
        'm.salahat@nusrv.com',
        'mkhader@nusrv.com'
    ),*/
);